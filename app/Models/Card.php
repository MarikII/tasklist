<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Card
 *
 * @package App\Models
 * @property string name
 * @property string description
 * @property string datetime
 */
class Card extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'description'];

    public function cardUsers()
    {
        return $this->hasMany(CardUser::class);
    }
}
