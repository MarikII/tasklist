<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CardUser
 *
 * @package App\Models
 * @property int card_id
 * @property int user_id
 * @property int column_id
 * @property int status_id
 */
class CardUser extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['card_id', 'user_id', 'column_id', 'status_id'];

    public function card()
    {
        return $this->belongsTo(Card::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function column()
    {
        return $this->belongsTo(Column::class);
    }
}
