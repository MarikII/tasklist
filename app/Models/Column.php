<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Column
 *
 * @package App\Models
 * @property int id
 * @property string name
 * @property int user_id
 * @property int position
 */
class Column extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'string', 'user_id', 'position'];

    public function cardUsers()
    {
        return $this->hasMany(CardUser::class);
    }

    public function users()
    {
        return $this->belongsTo(User::class);
    }
}
